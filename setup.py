from cx_Freeze import setup, Executable
import sys

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(
	packages = [],
	excludes = [],
	bin_includes = ['libpoppler-qt5.so.1']
)

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
	base = "Win32GUI"

executables = [
	Executable(
		'exec.py',
		base=base,
		targetName='thoth.exe'
	)
]

setup(
	name='thoth',
	version = '1.0',
	description = 'note editing program',
	options = dict(build_exe = buildOptions),
	executables = executables
)
