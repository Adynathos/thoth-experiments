from .QT import Qt
import os
from popplerqt5 import Poppler
from contextlib import ExitStack
from wand import image
from thoth.ui.DocumentViewport import DocumentViewport
from thoth.ui.factories.BasicElementFactory import BasicElementFactory
from thoth.document.TextElement import TextElement
from thoth.document.LineElement import LineElement
from thoth.document.RectElement import RectElement
from PyPDF2 import PdfFileReader, PdfFileWriter
import PyPDF2
import pdfrw

class MainWindow(Qt.QMainWindow):
	def __init__(self, parent=None):
		Qt.QMainWindow.__init__(self, parent)
		self.setWindowTitle('Thoth')
		self.resize(1024, 768)
		self.move(0, 0)

		actSave = Qt.QAction("&Save", self)
		actSave.setShortcuts(Qt.QKeySequence.Save);
		actSave.triggered.connect(self.slotSave)

		actQuit = Qt.QAction("&Quit", self)
		actQuit.setShortcuts(Qt.QKeySequence.Quit);
		actQuit.triggered.connect(Qt.QCoreApplication.instance().quit)

		fileMenu = self.menuBar().addMenu("&File")
		fileMenu.addAction(actSave)
		fileMenu.addAction(actQuit)

		background_doc = Poppler.Document.load("background.pdf")

		central_widget = Qt.QWidget(self)
		self.setCentralWidget(central_widget)
		central_layout = Qt.QVBoxLayout(central_widget)

		self.viewWidget = DocumentViewport(background_doc, central_widget)

		central_layout.addWidget(self.createTools())
		central_layout.addWidget(self.viewWidget)


# 		edit = Qt.QTextEdit()
# 		edit.setHtml("Edit this <b>rich</b> text")
# 		#reedit.setReadOnly(True)
# 		edit.setAutoFillBackground(False)
#
# 		editProxy = Qt.QGraphicsProxyWidget()
# 		editProxy.setWidget(edit)
# 		editProxy.setPos(0, 200)
# 		self.scene.addItem(editProxy)
#
# 		#lb = Qt.QLabel()
# 		#lb.setDocument(edit.document())
#
# 		text = self.scene.addText("text TTTT", font)
# 		text.setFlag(Qt.QGraphicsItem.ItemIsMovable)
# 		text.setPos(300, 200)
# 		text.setDocument(edit.document())

		#img = image.Image(filename="example.pdf")
		#print(self.imgWidget.sizePolicy().horizontalPolicy())

	def factorySettingFunction(self, factory):
		return lambda: self.viewWidget.setSelectedFactory(factory)

	def createTools(self):
		tool_widget = Qt.QWidget(self)
		tool_layout = Qt.QHBoxLayout(tool_widget)

		factories = [
			BasicElementFactory('Text', TextElement),
			BasicElementFactory('Line', LineElement),
			BasicElementFactory('Rect', RectElement),
		]

		for factory in factories:
			btn = Qt.QPushButton(factory.name, tool_widget)
			btn.clicked.connect(self.factorySettingFunction(factory))
			tool_layout.addWidget(btn)

		self.viewWidget.setSelectedFactory(factories[1])

		return tool_widget

	def slotSave(self):
		print("Save")

		out_filename, file_ext = Qt.QFileDialog.getSaveFileName(None, "Export PDF", "", "*.pdf")

		if not out_filename:
			return

		tmp_out_filename = out_filename + '.tmp'

		background_filename = "background.pdf"

		if os.path.isfile('background.pdf'):

			self.viewWidget.renderToPDF(tmp_out_filename)

			# merge
			with ExitStack() as stack:
				background_file = open(background_filename, 'rb')
				stack.enter_context(background_file)
				background_pdf = pdfrw.PdfReader(background_filename) #PdfFileReader(background_file)

				tmp_out_file = open(tmp_out_filename, 'rb')
				stack.enter_context(tmp_out_file)
				content_pdf = pdfrw.PdfReader(tmp_out_filename) #PdfFileReader(tmp_out_file)

				out_file = open(out_filename, 'wb')
				stack.enter_context(out_file)

				out_pdf = pdfrw.PdfWriter() #PdfFileWriter()

				pdfrw.PageMerge(background_pdf.pages[0]).add(content_pdf.pages[0]).render()

				out_pdf.write(out_file, background_pdf)

		else:
			self.viewWidget.renderToPDF(out_filename)

			#p0 = PyPDF2.pdf.PageObject.createBlankPage()
			#p0.mergePage(background_pdf.getPage(0))
			#p0 = background_pdf.getPage(0)
			#p0.mergePage(content_pdf.getPage(0))
			#out_pdf.addPage(p0)
			#out_pdf.write(out_file)



# 		with ExitStack() as stack:
# 			background_file = open(background_filename, 'rb')
# 			stack.enter_context(background_file)
# 			background_pdf = PdfFileReader(background_file)
#
# 			tmp_out_file = open(tmp_out_filename, 'rb')
# 			stack.enter_context(tmp_out_file)
# 			content_pdf = PdfFileReader(tmp_out_file)
#
# 			out_file = open(out_filename, 'wb')
# 			stack.enter_context(out_file)
#
# 			out_pdf = PdfFileWriter()
# 			#p0 = PyPDF2.pdf.PageObject.createBlankPage()
# 			#p0.mergePage(background_pdf.getPage(0))
# 			p0 = background_pdf.getPage(0)
# 			p0.mergePage(content_pdf.getPage(0))
# 			out_pdf.addPage(p0)
# 			out_pdf.write(out_file)
