'''
Created on 8 May 2016

@author: lisss
'''

from .ElementFactory import ElementFactory

class BasicElementFactory(ElementFactory):

	def __init__(self, name, element_class):
		super().__init__(name)
		self.element_class = element_class
				
	def createElement(self, page, click_position):
		new_elem = self.element_class(page)
		new_elem.graphicsInit()
		new_elem.moveControlPoint(new_elem.getInitialCreationControlPointId(), click_position)
		
		return new_elem
