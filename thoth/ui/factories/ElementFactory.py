
class ElementFactory:
	'''
	Stores frequently used element prototypes,
	like "header text", "body text", "red arrow"
	so that they can be quickly created during editing.
	'''
	
	def __init__(self, name):
		self.name = name
	
	def name(self):
		return self.name
	
	def createElement(self, page, click_position):
		raise NotImplemented("Abstract ElementFactory")
	