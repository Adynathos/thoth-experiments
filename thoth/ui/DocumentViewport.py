from .QT import Qt, QtPrintSupport

from .graphics.GraphicsItemWrapper import GraphicsItemWrapper
from random import randint, sample
from ..document.Document import Document
from ..document.DocumentElement import VisibilityMode
from ..document.Page import Page
from ..document.LineElement import LineElement
from ..document.TextElement import TextElement

class DocumentViewport(Qt.QGraphicsView):

	def __init__(self, pdf_document = None, parent = None):
		super().__init__(parent)

		self.setScene(Qt.QGraphicsScene())

		self.document = Document(self.scene(), pdf_document)

		self.mouse_drag_start = Qt.QPoint(0, 0)

		self.selected_element = None
		self.selected_cp_id = None
		self.selected_factory = None

		self.rectBrush = Qt.QBrush(Qt.QColor(0, 150, 50, 200))
		self.rectPen = Qt.QPen(Qt.QColor(0, 200, 50, 255))
		self.redBrush = Qt.QBrush(Qt.QColor(200, 50, 50, 255))

		self.document.graphicsInit()
		self.selected_page = self.document.pages[0]

		self.setScene(self.document.scene)
		self.setSceneRect(self.selected_page.geometry_rect)

		# display rendered pdf smoothly
		self.setRenderHint(Qt.QPainter.Antialiasing, True)
		self.setRenderHint(Qt.QPainter.SmoothPixmapTransform, True)

		self.scale(2, 2)

	def layoutExperiment(self):

		layout_vert = Qt.QGraphicsLinearLayout(2)
		self.main_layout = layout_vert

		self.layouts = [
			Qt.QGraphicsLinearLayout(layout_vert),
			Qt.QGraphicsLinearLayout(layout_vert),
			Qt.QGraphicsLinearLayout(layout_vert),
		]
		for lt in self.layouts:
			layout_vert.addItem(lt)

		layout_container = Qt.QGraphicsWidget()
		layout_container.setLayout(layout_vert)
		self.scene().addItem(layout_container)

	def setSelectedFactory(self, factory):
		self.selected_factory = factory

	def setSelectedElement(self, elem, cp):
		print(self.selected_element, '->', elem)

		if self.selected_element:
			self.selected_element.setSelection(False)
			self.selected_cp_id = None

		self.selected_element = elem
		self.selected_cp_id = cp

		if self.selected_element:
			self.selected_element.setSelection(True)

	def addRectToLayout(self):
		if randint(0, 1) == 2:
			r1 = Qt.QGraphicsRectItem(0, 0, 25, 25)
			r1.setBrush(Qt.QBrush(Qt.QColor(randint(0, 255), randint(0, 255), randint(0, 255), 255)))
			r1.setPen(self.rectPen)
		else:
			r1 = Qt.QGraphicsTextItem()
			r1.setPlainText("Qua")
			r1.setFont(Qt.QFont("Arial", 18))

		r1w = GraphicsItemWrapper(r1)
		sample(self.layouts, 1)[0].addItem(r1w)
		self.main_layout.updateGeometry()
		#layout.setStretchFactor(r1w, 1)

	def mousePressEvent(self, event):
		mouse_pos = self.mapToScene(event.x(), event.y())
		if event.button() == 1:
			clicked_item = self.scene().itemAt(mouse_pos, self.transform())
			if clicked_item:
				elem = clicked_item.data(0)

				print('Clicked', elem)

				if type(elem) is Page:
					if self.selected_factory:
						new_elem = self.selected_factory.createElement(self.selected_page, mouse_pos)
						self.setSelectedElement(new_elem, new_elem.getInitialDragControlPointId())
						self.selected_element.moveControlPoint(self.selected_cp_id, mouse_pos)
				else:
					self.setSelectedElement(elem, 0)
			else:
				print('Clicked on nothing')

		elif event.button() == 2:
			self.setSelectedElement(None, None)

		event.accept()


# 		new_line = LineElement(self.selected_page)
# 		new_line.graphicsInit()
# 		new_line.moveControlPoint(new_line.getInitialCreationControlPointId(), mouse_pos)



# 		#super().mousePressEvent(event)
# 		#return
#
# 		self.addRectToLayout()
# 		return
#
# 		self.mouseDragStart = self.mapToScene(event.x(), event.y())
#
# 		r = Qt.QGraphicsRectItem(self.mouseDragStart.x(), self.mouseDragStart.y(), 10, 10)
# 		r.setBrush(self.rectBrush)
# 		r.setPen(self.rectPen)
# 		self.scene().addItem(r)
# 		self.selectedRect = r
#
# 		font = Qt.QFont("Arial", 12)
# 		text = self.scene().addText("Box", font)
# 		text.setFlag(Qt.QGraphicsItem.ItemIsMovable)
# 		text.setPos(self.mouseDragStart)

		#font = Qt.QFont("Arial", 12)

		#text = self.scene().addText("text TTTT", font)
		#text.setFlag(Qt.QGraphicsItem.ItemIsMovable)
		#text.setPos(scene_pos)

	def mouseMoveEvent(self, event):
		mouse_pos = self.mapToScene(event.x(), event.y())

		if self.selected_element:
			if self.selected_cp_id is not None:
				# moving
				self.selected_element.moveControlPoint(self.selected_cp_id, mouse_pos)

	def mouseReleaseEvent(self, event):
		#mouse_pos = self.mapToScene(event.x(), event.y())

		self.selected_cp_id = None

	def renderToPDF(self, filename):
		self.setSelectedElement(None, None)

		printer = Qt.QPrinter(QtPrintSupport.QPrinter.PrinterResolution)
		printer.setOutputFormat(Qt.QPrinter.PdfFormat)
		printer.setPaperSize(self.document.pages[0].geometry_size, Qt.QPrinter.Point)
		# margins would slide the content in relation to background
		printer.setPageMargins(0, 0, 0, 0, Qt.QPrinter.Point)
		#printer.setOrientation(Qt.QPrinter.Landscape)
		printer.setOutputFileName(filename)

		self.document.setDisplayMode(VisibilityMode.PRINT)
		self.scene().render(Qt.QPainter(printer))
		self.document.setDisplayMode(VisibilityMode.EDITOR)
