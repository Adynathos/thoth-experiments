'''
Created on 27 Feb 2016

@author: lisss
'''

from PyQt5 import Qt
from PyQt5.QtWidgets import QGraphicsLayoutItem

class GraphicsItemWrapper(QGraphicsLayoutItem):
	'''
	classdocs
	'''
	def __init__(self, item):
		'''
		Constructor
		'''
		super().__init__()
		
		self.item = item
		self.setGraphicsItem(item)
		
	def sizeHint(self, which_size_type, constraint):
		# Extract the size of our QGraphicsItem
		
		if which_size_type == 0 or which_size_type == 1: #min or preferred
			rect = self.item.boundingRect()
			
			print(Qt.QSizeF(rect.width(), rect.height()))
			
			return Qt.QSizeF(rect.width(), rect.height())
		elif which_size_type == 2: #maximum
			return Qt.QSizeF(1000, 1000)
		
		return constraint
	
	def setGeometry(self, rect):
		# Apply the geometry to our QGraphicsItem
		super().setGeometry(rect)
		
		self.item.prepareGeometryChange()
		self.item.setPos(rect.topLeft())
		
		print("setGeometry", rect)