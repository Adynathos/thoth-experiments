'''
Created on 13 Mar 2016

@author: lisss
'''
from ..ui.QT import Qt
from .DocumentElement import DocumentElement

class RectElement(DocumentElement):
	'''
	classdocs
	'''

	def __init__(self, page):
		'''
		Constructor
		'''
		super().__init__(page)

		self.points = [Qt.QPointF(0, 0), Qt.QPointF(1, 1)]		
		
	def graphicsInit(self):
		super().graphicsInit()
		
		pen = Qt.QPen(Qt.QColor(100, 100, 200))		
		brush = Qt.QBrush(Qt.QColor(100, 100, 200, 50))
								
		#print(brush.style())
								
		self.rect_item = Qt.QGraphicsRectItem()
		self.rect_item.setPen(pen)
		self.rect_item.setBrush(brush)
		
		self.group.addToGroup(self.rect_item)
	
		
	def updateGeometry(self):
		self.group.setPos(self.points[0])
		p2 = self.points[1] - self.points[0]
		self.rect_item.setRect(0, 0, p2.x(), p2.y())
		
	def getInitialCreationControlPointId(self):
		return 0
	
	def getInitialDragControlPointId(self):
		return 1
		