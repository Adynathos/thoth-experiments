'''
Created on 12 Mar 2016

@author: lisss
'''
from ..ui.QT import Qt
from enum import IntEnum

class VisibilityMode(IntEnum):
	ROOT = 1
	PRINT = 2
	EDITOR = 3
	EDIT_CONTENT = 4
	EDIT_POSITION = 5
	EDIT_ROTATION = 6


# display categories:
	
# global display modes:
# PRINT, VIEW, EDITOR

# + specific modes for singular items, 
# for example when selected an overlay is drawn which could have EDITOR_SELECTED group


class DocumentElement(object):
	'''
	classdocs
	'''

	def __init__(self, page):
		'''
		Constructor
		'''
		self.page = page
		self.document = page.document
		
		if self.page:
			self.page.registerElement(self)

	def graphicsInit(self):
		self.group = Qt.QGraphicsItemGroup()
		self.group.setData(0, self)
		self.groups = {}
		
		self.groups[VisibilityMode.PRINT] = Qt.QGraphicsItemGroup()
		self.groups[VisibilityMode.PRINT].setData(0, self)
		self.group.addToGroup(self.groups[VisibilityMode.PRINT])
		
		self.groups[VisibilityMode.EDITOR] = Qt.QGraphicsItemGroup()
		self.groups[VisibilityMode.EDITOR].setData(0, self)
		self.group.addToGroup(self.groups[VisibilityMode.EDITOR])
		
		if self.page:
			self.page.group.addToGroup(self.group)
	
	def updateGeometry(self):
		print('Update geometry')
			
	def moveControlPointXY(self, point_id, x, y):
		return self.moveControlPoint(point_id, Qt.QPointF(x, y))
	
	def moveControlPoint(self, point_id, point):
		self.points[point_id] = point
		self.updateGeometry()
		
		return point_id
	
	def getInitialCreationControlPointId(self):
		return 0
	
	def getInitialDragControlPointId(self):
		return 1
	
	def setSelection(self, is_selected):
		# probably should have many selection modes
		self.selected = is_selected
	
	def setDisplayMode(self, mode):
		# There should be a list of graphics items which
		# are visible in each mode
		pass