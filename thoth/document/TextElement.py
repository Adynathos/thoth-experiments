'''
Created on 12 Mar 2016

@author: lisss
'''

from ..ui.QT import Qt, QtCore
from .DocumentElement import DocumentElement, VisibilityMode

class TextElement(DocumentElement):
	'''
	classdocs
	'''

	def __init__(self, page):
		'''
		Constructor
		'''
		super().__init__(page)


	def graphicsInit(self):
		super().graphicsInit()

		# text content
		self.textDocument = Qt.QTextDocument()
		self.textDocument.setHtml('<b>H</b>ello!')

		# object to display the text
		self.textItem = Qt.QGraphicsTextItem()
		self.textItem.setData(0, self)
		self.textItem.setDocument(self.textDocument)

		# object to edit the text
		self.textEditWidget = Qt.QTextEdit()
		self.textEditWidget.setDocument(self.textDocument)
		#self.textEditWidget.setReadOnly(False)
		print('read only', self.textEditWidget.isReadOnly())

		proxy = Qt.QGraphicsProxyWidget()
		proxy.setData(0, self)
		proxy.setFlag(Qt.QGraphicsItem.ItemIsFocusable, True)
		proxy.setWidget(self.textEditWidget)
		self.proxy = proxy

		self.groups[VisibilityMode.PRINT].addToGroup(self.textItem)
		self.groups[VisibilityMode.EDITOR].addToGroup(proxy)

	def moveControlPoint(self, point_id, point):
		self.group.setPos(point)

	def getInitialCreationControlPointId(self):
		return 0

	def getInitialDragControlPointId(self):
		return 0

	def setSelection(self, is_selected):
		print('TextElement.setSelection ', is_selected)
		super().setSelection(is_selected)
		self.groups[VisibilityMode.EDITOR].setVisible(is_selected)

		if is_selected:
			self.textEditWidget.setFocus()
			self.textEditWidget.grabKeyboard()
			print('TextElement.hasFocus?', self.textEditWidget.hasFocus())
		else:
			self.textEditWidget.releaseKeyboard()
