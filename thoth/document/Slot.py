'''
Created on 12 Mar 2016

@author: lisss
'''
from ..ui.QT import Qt

class Slot(object):
	'''
	classdocs
	'''

	def __init__(self, params):
		'''
		Constructor
		'''
		
class PointSlot(Slot):
	def __init__(self, x = 0, y = 0):
		self.position = Qt.QPointF(x, y)
			
class RectSlot(Slot):
	def __init__(self, x = 0, y = 0, w = 32, h = 32):
		self.rect = Qt.QRectF(x, y, w, h)
		
class LineSlot(Slot):
	def __init__(self, p1, p2):
		self.start = p1
		self.end = p2
		
