'''
Created on 12 Mar 2016

@author: lisss
'''
from ..ui.QT import Qt
from .DocumentElement import DocumentElement, VisibilityMode

class LineElement(DocumentElement):
	'''
	classdocs
	'''

	def __init__(self, page):
		'''
		Constructor
		'''
		super().__init__(page)
		
		self.points = [Qt.QPointF(0, 0), Qt.QPointF(1, 1)]
		
	def graphicsInit(self):
		super().graphicsInit()
		
		pen = Qt.QPen()
		pen.setColor(Qt.QColor(0, 200, 0, 255))
		pen.setWidth(2)
				
		self.line = Qt.QLineF()
				
		self.line_item = Qt.QGraphicsLineItem()
		self.line_item.setPen(pen)
		self.line_item.setData(0, self)
		
		self.groups[VisibilityMode.PRINT].addToGroup(self.line_item)
	
		
	def updateGeometry(self):
		self.group.setPos(self.points[0])
		p2 = self.points[1] - self.points[0]
		self.line.setP2(p2)
		self.line_item.setLine(self.line)
		
	def getInitialCreationControlPointId(self):
		return 0
	
	def getInitialDragControlPointId(self):
		return 1
		