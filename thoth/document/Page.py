'''
Created on 12 Mar 2016

@author: lisss
'''
from ..ui.QT import Qt
from .DocumentElement import DocumentElement, VisibilityMode

class Page(DocumentElement):
	'''
	classdocs
	'''

	SIZES = {
		'A4_L': Qt.QSizeF(842, 595),
		'A4_P': Qt.QSizeF(595, 842),
	}

	def __init__(self, document, size_id = 'A4_L'):
		'''
		Constructor
		'''
		self.document = document
		self.page = None

		self.elements = set()

		if self.document.pdf_document:
			self.pdf_page = self.document.pdf_document.page(0)
			self.geometry_size = self.pdf_page.pageSizeF()
			print('Page size ', self.geometry_size.width(), 'x', self.geometry_size.height())
		else:
			self.pdf_page = None
			self.geometry_size = self.SIZES[size_id]

		self.geometry_rect = Qt.QRectF(Qt.QPointF(0, 0), self.geometry_size)

	def graphicsInit(self):
		super().graphicsInit()

		self.document.scene.addItem(self.group)

		if self.pdf_page:
			pdf_pixmap = Qt.QPixmap.fromImage(self.pdf_page.renderToImage(300, 300))

			page_item = Qt.QGraphicsPixmapItem(pdf_pixmap)
			background_size = page_item.boundingRect()

			page_item.setScale(min(
				self.geometry_rect.width() / background_size.width(),
				self.geometry_rect.height() / background_size.height()))
		else:
			page_item = Qt.QGraphicsRectItem(self.geometry_rect)
			page_item.setBrush(Qt.QBrush(Qt.QColor(255, 255, 255, 255)))

		page_item.setPos(0,0)

		page_item.setData(0, self)
		self.page_background = page_item

		self.group.addToGroup(page_item)

	def registerElement(self, elem):
		self.elements.add(elem)

	def unregisterElement(self, elem):
		self.elements.remove(elem)

	def setDisplayMode(self, mode):
		for e in self.elements:
			e.setDisplayMode(mode)

		self.page_background.setVisible(mode != VisibilityMode.PRINT)
