'''
Created on 12 Mar 2016

@author: lisss
'''
from .Page import Page

class Document:
	'''
	classdocs
	'''

	def __init__(self, scene, pdf_document):
		'''
		Constructor
		'''
		
		self.pages = []
		self.scene = scene
		self.pdf_document = pdf_document
		
		self.addPage(Page(self))
		
	def addPage(self, page):
		self.pages.append(page)
				
	def graphicsInit(self):
		for p in self.pages:
			p.graphicsInit()
			
	def setDisplayMode(self, mode):
		for p in self.pages:
			p.setDisplayMode(mode)