#!/usr/bin/env python3
import sys
from .ui.QT import Qt

def main():
	from .ui.MainWindow import MainWindow

	qt_application = Qt.QApplication(sys.argv)

	window = MainWindow()
	window.show()

	qt_application.exec_()

if __name__ == '__main__':
	main()
